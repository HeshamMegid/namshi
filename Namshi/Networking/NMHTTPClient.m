//
//  NMHTTPClient.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "NMHTTPClient.h"

@implementation NMHTTPClient

static NSString * const NMHttpClientBaseURL = @"http://white-shadow-6965.getsandbox.com/";

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[NMHTTPClient alloc] init];
	});
    
	return sharedInstance;
}

#pragma mark - NSObject

- (id)init {
    NSURL *baseURL = [[NSURL alloc] initWithString:NMHttpClientBaseURL];
    
    if ((self = [super initWithBaseURL:baseURL])) {
		// Use JSON
		[self setDefaultHeader:@"Accept" value:@"application/json"];
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setParameterEncoding:AFFormURLParameterEncoding];
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
	}
    
	return self;
}

#pragma mark - Controller methods

- (void)productsWithCount:(NSUInteger)count
            fromProductID:(NSUInteger)productID
                  success:(void (^)(NSArray *products))success
                  failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"from" : @(productID),
                                 @"count" : @(count)};
    
    [self getPath:@"products"
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSMutableArray *products = [[NSMutableArray alloc] init];
              
              for (NSDictionary *productDictionary in responseObject) {
                  [products addObject:[NMProduct objectWithDictionary:productDictionary]];
              }
              
              if (success) {
                  success(products);
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
}

@end
