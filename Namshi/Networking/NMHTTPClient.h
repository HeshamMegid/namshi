//
//  NMHTTPClient.h
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "NMProduct.h"

@interface NMHTTPClient : AFHTTPClient

+ (instancetype)sharedInstance;

- (void)productsWithCount:(NSUInteger)count
            fromProductID:(NSUInteger)productID
                  success:(void (^)(NSArray *products))success
                  failure:(void (^)(NSError *error))failure;
@end
