//
//  NMProduct.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "NMProduct.h"

@implementation NMProduct

- (NSDictionary *)mapping {
    return @{@"productID" : @"id",
             @"sku" : @"sku",
             @"name" : @"productName",
             @"brandName" : @"brandName",
             @"imageURL" : @"image",
             @"price" : @"price",
             @"page" : @"productPage"
             };
}

@end
