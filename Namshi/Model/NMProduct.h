//
//  NMProduct.h
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMMappableObject.h"

@interface NMProduct : NMMappableObject

@property (nonatomic, assign) NSUInteger productID;
@property (nonatomic, copy) NSString *sku;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *brandName;
@property (nonatomic, copy) NSString *imageURL;
@property (nonatomic, assign) NSUInteger price;
@property (nonatomic, copy) NSString *page;

@end
