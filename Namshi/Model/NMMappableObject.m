//
//  NMMappableObject.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "NMMappableObject.h"

@implementation NMMappableObject

+ (instancetype)objectWithDictionary:(NSDictionary *)dictionary {
    id object = [[[self class] alloc] init];
    [object unpackDictionary:dictionary];
    return object;
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    NSDictionary *mapping = [self mapping];
  
    [mapping enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [self setValue:dictionary[obj] forKey:key];
    }];
}

- (NSDictionary *)mapping {
    return nil;
}

@end
