//
//  NMMappableObject.h
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMMappableObject : NSObject

+ (instancetype)objectWithDictionary:(NSDictionary *)dictionary;

// Subclasses must override to return mapping in this format: {property: jsonKey}
- (NSDictionary *)mapping;

@end
