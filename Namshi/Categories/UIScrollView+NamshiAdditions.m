//
//  UIScrollView+NamshiAdditions.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "UIScrollView+NamshiAdditions.h"

@implementation UIScrollView (NamshiAdditions)

- (BOOL)isAtBottom {
    return ([self verticalOffsetForBottom] - self.contentOffset.y <= 100.0f);
}

- (CGFloat)verticalOffsetForBottom {
    CGFloat scrollViewHeight = self.bounds.size.height;
    CGFloat scrollContentSizeHeight = self.contentSize.height;
    CGFloat bottomInset = self.contentInset.bottom;
    CGFloat scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
    return scrollViewBottomOffset;
}

@end
