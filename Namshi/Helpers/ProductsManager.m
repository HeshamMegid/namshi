//
//  ProductsManager.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "ProductsManager.h"
#import <AutoCoding/AutoCoding.h>
#import "NMHTTPClient.h"

@interface ProductsManager()

@property (nonatomic, assign) NSUInteger page;

@end

@implementation ProductsManager

static const NSUInteger ProductsPerPage = 10;

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    static ProductsManager *sharedInstance = nil;
	if (sharedInstance == nil) {
        NSString *path = [[self documentsDirectory] stringByAppendingPathComponent:@"Products.plist"];
        sharedInstance = [ProductsManager objectWithContentsOfFile:path];
        sharedInstance.page = 0;
        
		if (sharedInstance == nil)        {
            sharedInstance = [[ProductsManager alloc] init];
		}
	}
    
	return sharedInstance;
}

#pragma mark - NSObject

- (id)init {
    if (self = [super init]) {
        self.products = [[NSArray alloc] init];
        self.page = 0;
    }
    
    return self;
}

#pragma mark - Controller methods

+ (NSString *)documentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

- (void)save {
	NSString *path = [[[self class] documentsDirectory] stringByAppendingPathComponent:@"Products.plist"];
    [self writeToFile:path atomically:YES];
}

- (void)productsWithSuccess:(void (^)(NSArray *products, ProductsManagerSource source))success
                    failure:(void (^)(NSError *error))failure {
    if ([self.products count] > 0 && self.page == 0) {
        if (success) {
            success(self.products, ProductsManagerSourceCache);
        }
    }
    
    [[NMHTTPClient sharedInstance] productsWithCount:ProductsPerPage
                                       fromProductID:self.page * ProductsPerPage
                                             success:^(NSArray *products) {
                                                 if (self.page == 0) {
                                                     // Replace the local cache if we're loading the first page.
                                                     self.products = products;
                                                 } else {
                                                     // Apend to the cache if we're loading pages past the first one.
                                                     NSMutableArray *mutableProducts = [[NSMutableArray alloc] initWithArray:self.products];
                                                     [mutableProducts addObjectsFromArray:products];
                                                     self.products = mutableProducts;
                                                 }
                                                 
                                                 [self save];
                                                 
                                                 if (success) {
                                                     success(self.products, ProductsManagerSourceNetwork);
                                                 }
                                                 
                                                 self.page = self.page + 1;
                                             } failure:^(NSError *error) {
                                                 if (failure) {
                                                     failure(error);
                                                 }
                                             }];
}

@end
