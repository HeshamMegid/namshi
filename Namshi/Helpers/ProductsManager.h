//
//  ProductsManager.h
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ProductsManagerSource) {
    ProductsManagerSourceCache,
    ProductsManagerSourceNetwork
};

@interface ProductsManager : NSObject

@property (nonatomic, strong) NSArray *products;

+ (instancetype)sharedInstance;
//- (void)save;

- (void)productsWithSuccess:(void (^)(NSArray *products, ProductsManagerSource source))success
                    failure:(void (^)(NSError *error))failure;

@end
