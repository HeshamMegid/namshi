//
//  WebViewController.h
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSURL *productURL;

@end
