//
//  WebViewController.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.productURL];
    [self.webView loadRequest:request];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    if ([url.scheme isEqual:@"namshi"]) {
        NSString *query = url.query;
        
        NSArray *pairs = [query componentsSeparatedByString:@"="];
        if ([[pairs firstObject] isEqualToString:@"message"]) {
            [self showAlertWithMessage:[pairs lastObject]];
        }
        
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - Controller methods

- (void)showAlertWithMessage:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Okay"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

@end
