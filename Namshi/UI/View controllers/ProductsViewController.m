//
//  ProductsViewController.m
//  Namshi
//
//  Created by Hesham Abd-Elmegid on 14/9/14.
//  Copyright (c) 2014 Hesham Abd-Elmegid. All rights reserved.
//

#import "ProductsViewController.h"
#import "NMHTTPClient.h"
#import "ProductsManager.h"
#import "UIScrollView+NamshiAdditions.h"
#import "WebViewController.h"

@interface ProductsViewController()

@property (nonatomic, strong) NSArray *products;
@property (nonatomic, assign) BOOL loadingProducts;

@end

@implementation ProductsViewController

static NSString * const ProductCellIdentifier = @"ProductCellIdentifier";
static NSString * const PushWebViewSegueIdentifier = @"PushWebViewSegueIdentifier";

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadProducts];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:PushWebViewSegueIdentifier]) {
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        NMProduct *product = self.products[selectedIndexPath.row];
        
        WebViewController *webViewController = (WebViewController *)segue.destinationViewController;
        webViewController.productURL = [NSURL URLWithString:product.page];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NMProduct *product = self.products[indexPath.row];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProductCellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = product.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Price: $%lu", (unsigned long)product.price];
    
    cell.imageView.image = nil;
    NSURL *url = [NSURL URLWithString:product.imageURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    __weak typeof(cell) weakCell = cell;
    
    [cell.imageView setImageWithURLRequest:request
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakCell.imageView.image = image;
                                       [weakCell setNeedsLayout];
                                   } failure:nil];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:PushWebViewSegueIdentifier sender:self];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.tableView isAtBottom]) {
        [self loadProducts];
    }
}

#pragma mark - Controller methods

- (void)loadProducts {
    if (!self.loadingProducts) {
        self.loadingProducts = YES;
        [[ProductsManager sharedInstance] productsWithSuccess:^(NSArray *products, ProductsManagerSource source) {
            if (source == ProductsManagerSourceNetwork) {
                NSLog(@"Objects loaded from network");
            } else {
                NSLog(@"Objects loaded from local cache");
            }
            
            self.products = products;
            [self.tableView reloadData];
            
            self.loadingProducts = NO;
        } failure:^(NSError *error) {
            NSLog(@"An error has occured %@", error.localizedDescription);

            self.loadingProducts = NO;
        }];
    }
}

@end
